<?php

@include '../Controller/GlobalVariable.php';

@require_once($_SERVER['DOCUMENT_ROOT'] . '/'.$PROJECTNAME.'/configuration.php');

$db = mysqli_connect(
    $HOST     == "" ? 'localhost': $HOST,
    $USER     == "" ? 'root'     : $USER,
    $PASSWORD == "" ? ''         : $PASSWORD,
    $DATABASE
);  

function register($data)
{
    global $db;

    $username  = strtolower($data["username"]);
    $email     = strtolower($data["email"]);
    $password  = mysqli_real_escape_string($db, $data["password"]);
    $password2 = mysqli_real_escape_string($db, $data["password2"]);

    $cekUsername = mysqli_query($db, "SELECT username FROM users WHERE username='$username'");

    if (mysqli_fetch_assoc($cekUsername)) {
        echo "<script>
            alert('username sudah terdaftar');
            </script>";
        return false;
    }

    $cekEmail = mysqli_query($db, "SELECT email FROM users WHERE email='$email'");
    
    if (mysqli_fetch_assoc($cekEmail)) {
        echo "<script>
            alert('email sudah terdaftar');
            </script>";
        return false;
    }

    if ($password !== $password2) {
        echo "<script>
            alert('password dan konfirmasi salah');
            </script>";
        return false;
    }

    $password = password_hash($password, PASSWORD_DEFAULT);

    mysqli_query($db, "INSERT INTO users VALUES('','$username','$email','$password')");

    return mysqli_affected_rows($db);
}

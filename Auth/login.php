<?php

    session_start();
    @include '../Controller/GlobalVariable.php';
    @include '../configuration.php';
    require "function.php";

    if (!$AUTH) {
        header("Location:../views/demo/data.php");
    }

    if (isset($_POST["submit"])) {
        $username = $_POST["username"];
        $password = $_POST["password"];

        $result = mysqli_query($db, "SELECT * FROM users WHERE username='$username'");

        if (mysqli_num_rows($result) === 1) {
            $Pwuser = mysqli_fetch_assoc($result);
            if (password_verify($password, $Pwuser["password"])) {
                $_SESSION['login']    = true;
                $_SESSION['username'] = $username;
                $_SESSION['id']       = $Pwuser['id'];
                $_SESSION['email']    = $Pwuser['email'];
                header("Location:../views/demo/data.php");
                exit;
            }
        }

        $error = true;
    }



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/dist/css/adminlte.min.css">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <title>login</title>
</head>
<style>
    .abu-abu{
        background-color: #2c3e50;
    }
    .margin{
        margin-top:120px;
    }
</style>
<body>

    <?php if (isset($error)) {
        echo '<div style="display:none"></div>';
        echo '<script type="text/javascript">
                  Swal.fire({
                      icon: "error",
                      title: "Opps!",
                      text: "Username belum terdaftar!",
                  })
            </script>';
    } ?>
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-3 margin">
                <div class="card">
                    <div class="card-header abu-abu">
                        <img src="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/dist/img/pandoracode.png" style="max-width: 200px;" alt="">
                    </div>
                    <div class="card-body">
                        <form action="" method="POST">
                            <label for="exampleInputEmail1">Username</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fa fa-user"></i></div>
                                </div>
                                <input type="Username" class="form-control" name="username" id="exampleInputEmail1" aria-describedby="UsernameHelp" placeholder="Masukan Username">
                            </div>
                            <label for="exampleInputEmail1">Password</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fa fa-key"></i></div>
                                </div>
                                <input type="password" name="password" class="form-control" id="password" placeholder="Password" autocomplete="off">
                            </div>
                            <div class="float-right">
                                <button type="submit" name="submit" class="btn btn-primary">Masuk</button>
                                <a class="btn btn-dark" href="register.php">Daftar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
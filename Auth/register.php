<?php
require_once 'function.php';

if (isset($_POST["submit"])) {
    if (register($_POST) > 0) {
        echo "<script>
        alert('registrasi berhasil');
        </script>";
        session_start();
        header("Location:login.php");

        @$getId = mysqli_query($db, "SELECT * FROM users WHERE username = '$_POST[username]' ");
        @$get   = mysqli_fetch_assoc($getId);

        $_SESSION['login']    = true;
        $_SESSION['username'] = $_POST['username'];
        $_SESSION['email']    = $_POST['email'];
    } else {
        echo mysqli_error($db);
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/dist/css/adminlte.min.css">
    <title>Register</title>
</head>

<style>
    .abu-abu{
        background-color: #2c3e50;
    }
    .margin{
        margin-top:120px;
    }
</style>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-3 margin">
                <div class="card">
                    <div class="card-header abu-abu">
                        <img src="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/dist/img/pandoracode.png" style="max-width: 200px;" alt="">
                    </div>
                    <div class="card-body">
                        <form action="" method="POST">
                            <label for="exampleInputEmail1">Username</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fa fa-user"></i></div>
                                </div>
                                <input type="Username" class="form-control" name="username" id="exampleInputEmail1" aria-describedby="UsernameHelp" placeholder="Masukan Username">
                            </div>
                            <label for="exampleInputEmail1">Email</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fa fa-envelope"></i></div>
                                </div>
                                <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="EmailHelp" placeholder="Masukan Email">
                            </div>
                            <label for="exampleInputEmail1">Password</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fa fa-key"></i></div>
                                </div>
                                <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                            </div>
                            <label for="exampleInputEmail1">Konfirmasi Password</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fa fa-key"></i></div>
                                </div>
                                <input type="password" name="password2" class="form-control" id="password2" placeholder="Konfirmasi Password">
                            </div>
                            <span>sudah punya akun? <a href="login.php">masuk</a></span>
                            <div class="float-right">
                                <button type="submit" name="submit" class="btn btn-primary">Daftar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>;
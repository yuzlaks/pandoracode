<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<?php

@include '../Controller/GlobalVariable.php';

@require_once($_SERVER['DOCUMENT_ROOT'] . '/'.$PROJECTNAME.'/configuration.php');

error_reporting(0);

$GLOBALS['koneksi'] = 
 
mysqli_connect(
    $HOST     == "" ? 'localhost': $HOST,
    $USER     == "" ? 'root'     : $USER,
    $PASSWORD == "" ? ''         : $PASSWORD,
    $DATABASE
);  

$GLOBALS['database'] = $DATABASE;

//Notif
if (@$CHECKDB) {
    if ($GLOBALS['koneksi']) {
        echo '<div style="display:none"></div>';
        echo ' <script type="text/javascript">
                    Swal.fire({
                        icon: "success",
                        title: "Berhasil!",
                        text: "Berhasil konek dengan database '.$DATABASE.'",
                    })
               </script> ';
    }else{
        echo '<div style="display:none"></div>';
        echo ' <script type="text/javascript">
                    Swal.fire({
                        icon: "error",
                        title: "Oops...",
                        text: "Gagal konek dengan database '.$DATABASE.'",
                    })
                </script> ';
                
    }
}
<?php include '../setting/app.php' ?>
<?php include '../layouts/template-header.php'; ?>

<form action="<?= route($store,$page, $id) ?>" method="POST">

    <div class="form-row">
        <div class="col-md-6 mb-3">
            <label>Nama</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-user"></i></span>
                </div>
                <input value="<?= @$list['nama'] ?>" type="text" name="nama" class="form-control" placeholder="Nama">
            </div>
        </div>
        <div class="col-md-6 mb-3">
            <label>Kota</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-city"></i></span>
                </div>
                <select name="kota_id" id="" class="form-control">
                    <option disabled selected>- Pilih Kota -</option>
                    <?php
                        $tabel = ambilTabel('kota');
                        while ($dataKota = ambilData($tabel)) {
                    ?>
                        <option <?= checkSama(@$list['kota_id'],$dataKota['id']) ?> value="<?= $dataKota['id'] ?>"><?= $dataKota['nama'] ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="col-md-12 mb-3">
            <label>Alamat</label>
            <div class="input-group">
                <textarea name="alamat" placeholder="Masukkan alamat..." class="form-control" id="" cols="30" rows="4"><?= @$list['alamat'] ?></textarea>
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="col-md-4 mb-3">
            <label>Nomor Telepon</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-phone"></i></span>
                </div>
                <input value="<?= @$list['no_telepon'] ?>" placeholder="08xxxxxxxxxx" type="text" name="no_telepon" class="form-control">
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="col-md-4 mb-3">
            <label>Jenis Kelamin</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-restroom"></i></span>
                </div>
                <select name="jenis_kelamin" id="" class="form-control">
                    <option disabled selected>- Pilih jenis kelamin -</option>
                    <option value="Laki-laki">Laki-laki</option>
                    <option value="Perempuan">Perempuan</option>
                </select>
            </div>
        </div>
    </div>

    <?php include '../layouts/buttons.php' ?>

</form>

<?php include '../layouts/template-footer.php' ?>
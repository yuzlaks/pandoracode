<?php include '../setting/app.php' ?>
<?php include '../layouts/template-header.php'; ?>

<a onclick="form('<?= $page ?>')" class="btn btn-primary btn-sm mb-3">Tambah Data</a>
<form action="<?= deleteMany($page) ?>" method="POST" id="form-delete">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Kota</th>
                <th>Alamat</th>
                <th>Nomor Telepon</th>
                <th>Jenis Kelamin</th>
                <th>Aksi</th>
                <th width="50px"><input type="checkbox" class="ml-3" id="check-all"></th>
            </tr>
        </thead>
        <tbody>
            <?php
                $table       = @relation('demo','kota', '*,kota.nama AS nama_kota, demo.nama AS nama_demo');
                while ($list = @ambilData($table)) {
            ?>

            <tr>

                <td><?= $no++ ?></td>
                <td><?= $list['nama_demo'] ?></td>
                <td><?= $list['nama_kota'] ?></td>
                <td><?= $list['alamat'] ?></td>
                <td><?= $list['no_telepon'] ?></td>
                <td><?= $list['jenis_kelamin'] ?></td>
                
                <!-- Tombol Edit & Checklist Akan Otomatis Terbuat -->
                <?php include '../layouts/buttons.php'?>

            </tr>
            
            <?php } ?>
        </tbody>

        <?= tombolHapus(7) ?>
        <!-- ubah angka mengikuti jumlah kolom table anda -->
        
    </table>
</form>

<?php include '../layouts/template-footer.php' ?>
<?php

    @include '../../Controller/GlobalVariable.php';
    @include '../Controller/GlobalVariable.php';
    @include 'Controller/GlobalVariable.php';

    @include '../../configuration.php';
    @include '../configuration.php';
    @include 'configuration.php';

    @$url    = explode("/",@$_SERVER['REQUEST_URI']);
    
    @$page = $url[3];

    @$location = @$url[3];
    
    @$url[3] = ucfirst($url[3]);
    @$url[3] = str_replace("_"," ", ucfirst($url[3]));


    @$forEdit = explode("?", $url[4]);

    if (@$url[4] == "data.php" || @$url[4] == "form.php" || @$url[4] == "profil.php" ) {
        if ($AUTH == TRUE) {
            session_start();
            if (empty($_SESSION['login'])) {
                header('location:../../Auth/login.php');
                exit;
            }
        }
    }
    
?>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>

    function hapus(table,id) {
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Data akan terhapus",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya'
          }).then((result) => {
            if (result.isConfirmed) {
              location.replace("../../Controller/GlobalController.php?delete="+id+"&table="+table)
            }
          })
    }

    function edit(table,id) {
        location.replace("../../Controller/GlobalController.php?edit="+id+"&table="+table)
    }

    function detail(table,id,detail) {
        location.replace("../../Controller/GlobalController.php?edit="+id+"&table="+table+"&mode=detail")
    }

    function form(folder) {
        location.replace("../"+folder+"/form.php");
    }

    function back(folder) {
        location.replace("../"+folder+"/data.php");
    } 

</script>

<?php

    @$PARAMS          = $_GET['params'];
    @$id              = $_GET['id'];
    @$data            = $_GET['query'];
    @$controller      = $_GET['controller'];
    @$function        = $_GET['function'];
    
    @$deleteMany      = $_GET['deleteMany'];
    @$valueDeleteMany = $_POST['deleteMany'];
    @$columnTable     = $_GET['column-table'];
    @$targetFolder    = $_GET['target-folder'];
    
    @$request     = null;
    @$nameOnly    = null;
    @$nameOfField = null;
    @$store       = null;
    @$file        = null;
    @$nameOfFile  = null;

    // print_r($EXPLODE);

    if (@$deleteMany) {
        
        $valueDeleteMany = implode(",", $valueDeleteMany);
        header('location:../../Controller/GlobalController.php?deleteMany=true&params='.$PARAMS.'&request='.$valueDeleteMany.'&column-table='.$columnTable.'&target-folder='.$targetFolder);

        die();
    }


    foreach ($_POST as $key => $value) {
        $request       .= htmlspecialchars($value)."|";
        $nameOfField   .= htmlspecialchars($key)."="."'".htmlspecialchars($value)."'".",";
        $nameOnly      .= htmlspecialchars($key).",";
    }


    $request     = rtrim($request, "|");
    $nameOfField = str_replace(",kirim='',","",$nameOfField);

    if (@$PARAMS == null) {

        include $_SERVER['DOCUMENT_ROOT'].'/'.$PROJECTNAME.'/'.'Controller/GlobalController.php';

    }else{

        header('location:../../Controller/GlobalController.php?function='.$function.'&params='.$PARAMS.'&request='.$request.'&data-update='.$nameOfField.'&id='.$id.'&nameonly='.$nameOnly.'&file='.$file.'&namefile='.$nameOfFile);

    }

    if (@$controller) {
        header('location:../../Controller/'.$controller.'.php?function='.$function.'&id='.$id.'&request='.$nameOfField);
    }


    if ($forEdit[0] == "form.php" && $data != null) {

        @$tabel = queryEdit(base64_decode($data));
        @$list  = ambilData($tabel);
    }

    if ($data) {
        $store = "update";
    }else{
        $store = "tambah";
    }

<?php

    include '../../configuration.php';

    $GLOBALS['koneksi'] = 
    
    mysqli_connect(
        $HOST     == "" ? 'localhost': $HOST,
        $USER     == "" ? 'root'     : $USER,
        $PASSWORD == "" ? ''         : $PASSWORD,
        $DATABASE
    );  

    function updateUser($data, $id)
    {
    
        $username  = strtolower($data["username"]);
        $email     = strtolower($data["email"]);
        $password  = mysqli_real_escape_string($GLOBALS['koneksi'], $data["password"]);
        $password2 = mysqli_real_escape_string($GLOBALS['koneksi'], $data["password2"]);

        $getData    = mysqli_query($GLOBALS['koneksi'],"SELECT * FROM users WHERE id = '$id'");
        $list       = mysqli_fetch_assoc($getData);

        if (empty($data['password'])) {
            $password = $list['password'];
        }else{
            
            if ($password !== $password2) {
                echo '<div style="display:none"></div>';
                echo '<script type="text/javascript">
                        Swal.fire({
                            icon: "error",
                            title: "Oops!",
                            text: "Konfirmasi tidak sama !",
                        }).then(function() {
                            window.location = "profil.php";
                        });
                    </script>';
                exit();
            }else{
                $password = password_hash($password, PASSWORD_DEFAULT);
            }
        }
        

        $cekUsername = mysqli_query($GLOBALS['koneksi'],"SELECT * FROM users WHERE username = '$username'");
        $cekEmail    = mysqli_query($GLOBALS['koneksi'],"SELECT * FROM users WHERE email    = '$email'");


        if ($list['username'] != $username) {
            if (mysqli_num_rows($cekUsername) > 0) {
                echo '<div style="display:none"></div>';
                echo '<script type="text/javascript">
                        Swal.fire({
                            icon: "error",
                            title: "Oops!",
                            text: "Username sudah dipakai !",
                        }).then(function() {
                            window.location = "profil.php";
                        });
                    </script>';
                exit();
            }
        }

        if ($list['email'] != $email) {
            if (mysqli_num_rows($cekEmail) > 0) {
                echo '<div style="display:none"></div>';
                echo '<script type="text/javascript">
                        Swal.fire({
                            icon: "error",
                            title: "Oops!",
                            text: "Email sudah dipakai !",
                        }).then(function() {
                            window.location = "profil.php";
                        });
                    </script>';
                exit();
            }
        }

        $update = mysqli_query($GLOBALS['koneksi'], "UPDATE users SET username = '$username', email = '$email', password = '$password' WHERE id = $id ");
        if ($update) {
            echo '<div style="display:none"></div>';
            echo '<script type="text/javascript">
                    Swal.fire({
                        icon: "success",
                        title: "Berhasil!",
                        text: "Berhasil diupdate !",
                    }).then(function() {
                        window.location = "profil.php";
                    });
                </script>';
            exit();
        }
        
        
    }    

    function checkPass($pass, $passwordBefore)
    {

        echo '<div style="display:none"></div>';
        echo "<script type='text/javascript'>
                (async () => {
                            
                        const inputValue = ''
                        
                        const { value: passwordSebelumnya } = await Swal.fire({

                            title           : 'Masukkan password lama',
                            input           : 'text',
                            inputLabel      : 'Untuk meyakinkan bahwa ini adalah akun anda',
                            inputValue      : inputValue,
                            showCancelButton: true,

                                inputValidator: (value) => {
                                    if (!value) {
                                    return 'You need to write something!'
                                    }
                                }
                        
                        })";

                        $test = password_verify($pass, $passwordBefore);

        echo           " ($test == 1) ? console.log('bener') : console.log('salah');
                })()

        </script>";                
    }
?>
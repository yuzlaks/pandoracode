<?php

    require_once 'function.php';
    include '../setting/app.php';
    include '../layouts/template-header.php';

    $tabel = rawQuery("SELECT * FROM users WHERE id = '$_SESSION[id]' ");
    $list  = ambilData($tabel);

    if (isset($_POST['submit'])) {
        if (updateUser($_POST, $list['id']) > 0) {
            echo '<div style="display:none"></div>';
            echo '<script type="text/javascript">
                    Swal.fire({
                        icon: "success",
                        title: "Berhasil!",
                        text: "Data berhasil diperbaruhi",
                    }).then(function() {
                        window.location = "profil.php";
                    });
                </script>';
        } else {
            // echo mysqli_errno($GLOBALS['koneksi']);
        }
    }

?>

<form action="" method="POST">

    <label for="">Username <span class="merah">*</span></label>
    <div class="input-group mb-2">
        <div class="input-group-prepend">
          <div class="input-group-text"><i class="fa fa-user"></i></div>
        </div>
        <input placeholder="Username" type="text" class="form-control" value="<?= $list['username'] ?>" name="username">
    </div>

    <label for="">Email <span class="merah">*</span></label>
    <div class="input-group mb-2">
        <div class="input-group-prepend">
          <div class="input-group-text"><i class="fa fa-envelope"></i></div>
        </div>
        <input placeholder="Jhon@jhon.com" type="text" class="form-control" value="<?= $list['email'] ?>" name="email">
    </div>

    <label for="">Password <span class="merah">*</span></label>
    <div class="input-group mb-2">
        <div class="input-group-prepend">
          <div class="input-group-text"><i class="fa fa-key"></i></div>
        </div>
        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
    </div>

    <label for="exampleInputEmail1">Konfirmasi Password</label>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
        <div class="input-group-text"><i class="fa fa-key"></i></div>
        </div>
        <input type="password" name="password2" class="form-control" id="password2" placeholder="Konfirmasi Password">
    </div>

    <div class="float-right">
        <button type="submit" name="submit" class="btn btn-primary btn-sm mt-3">Update</button>
    </div>


</form>

<?php include '../layouts/template-footer.php' ?>
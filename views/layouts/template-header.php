<?php @include '../../Controller/GlobalVariable.php'; ?>

<?php
    $url = explode("/",@$_SERVER['REQUEST_URI']);
    $url[3] = ucfirst($url[3]);
    $url[3] = str_replace("_"," ", ucfirst($url[3]));

    $location = strtolower($url[3]);

    @$forEdit = explode("?", $url[4]);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>PANDORACODE</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/plugins/summernote/summernote-bs4.min.css">
  <!-- Datatable -->
  <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/plugins/dataTables.bootstrap4.min.css">
</head>

<style>
  .merah{
    color:red;
  }
</style>

<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

   <?php
      if (!empty(@$_SESSION['login'])) {
        
      $tabel = rawQuery("SELECT * FROM users WHERE id = '$_SESSION[id]' ");
      $list  = ambilData($tabel);
    ?>
       <ul class="navbar-nav ml-auto">
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown ">
            <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="true">
              <i class="far fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right " style="left: inherit; right: 0px;">
              <span class="dropdown-item dropdown-header">Login Menggunakan</span>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <i class="fas fa-user mr-2"></i> <?= $list['username'] ?>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <i class="fas fa-envelope mr-2"></i> <?= $list['email'] ?>
              </a>
              <div class="dropdown-divider"></div>

              <?php
                if ($url[4] == "profil.php") {
                  ?>
                  
              <?php
                }else{
              ?>
                <a href="../users/profil.php" class="btn btn-primary btn-sm mx-3 my-3 float-left">Profil</a>
              <?php
                }
              ?>
                <a href="../../Auth/logout.php" class="btn btn-dark btn-sm mx-3 my-3 float-right">Keluar</a>
            </div>
          </li>
        </ul>
    <?php
      }
   ?>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <center>
        <img src="http://<?= $_SERVER['HTTP_HOST'] ?>/<?= $PROJECTNAME ?>/views/layouts/dist/img/pandoracode.png" alt="AdminLTE Logo" class="brand-image mt-2" style="opacity: .8;max-width:200px"><br>
      </center>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <br>
      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Cari Halaman" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <?php include 'template-menu.php' ?>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content --><br>
    <section class="content">
      <div class="container-fluid">
        <!-- <div class="row">
          <div class="col-lg-3 col-6">
            <div class="small-box bg-info">
              <div class="inner">
                <h3>150</h3>

                <p>New Orders</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div> -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">

                  

                  <?php 

                    if ($url[4] == "data.php") {
                      echo "Data ".str_replace("_"," ",$url[3]);
                    }

                    if ($url[4] == "form.php") {
                      echo "Tambah ".str_replace("_"," ",$url[3]);
                    }

                    if ($url[4] == "profil.php") {
                      echo "Profil user";
                    }

                    if (@$forEdit[0] == "edit.php" || @$_GET['query'] && empty(@$_GET['mode'])) {
                      echo "Edit ".str_replace("_"," ",$url[3]);
                    }

                    if (@$_GET['mode']) {
                      echo "Lihat ".str_replace("_"," ",$url[3]);
                    }
                  
                  ?>
                </h3>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content p-0">
                  
<li class="nav-item menu-open">
    <a href="#" class="nav-link active">
        <i class="nav-icon fas fa-cube"></i>
        <p>
            Master
            <i class="right fas fa-angle-left"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">

        <li class="nav-item">

            <?php

                $automaticCreateMenu = TRUE; 
                //Ubah jadi FALSE jika ingin membuat menu sendiri
                //Jika anda mengaktifkan fitur ini maka menu akan otomatis terbuat, tapi dengan urutan yang acak
                include 'automaticMenu.php';

            ?>

            <!-- Perbanyak bagian ini -->

            <!-- <a href='../demo/data.php' class='nav-link <?= $url[3] == 'demo' ? 'active' : ''; ?>'>
                <i class='far fa-circle nav-icon'></i>
                <p>Demo</p>
            </a> -->

        </li>
        
    </ul>

</li>
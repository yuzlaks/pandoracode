<?php
    @include '../../Controller/GlobalVariable.php';

    $url         = explode("/",@$_SERVER['REQUEST_URI']);

    $directory   = $_SERVER['DOCUMENT_ROOT'] . '/'.$PROJECTNAME.'/views/';

    if (!is_dir($directory)) {
        exit('Invalid diretory path');
    }

    if ($automaticCreateMenu == TRUE) {
        $files = array();
        foreach (scandir($directory) as $file) {
            if ($file !== '.' && $file !== '..') {
                $files[] = $file;

            }
        }

        foreach( $files as $key => $value){

            $active = $url[3] == $value ? 'active' : '';

            $attr   = $value  == "layouts" || $value  == "users" || $value  == "setting" || $value  == "menu_level" ? 'style="display:none"' : '';

            $data = "<a $attr href='../$value/data.php' class='nav-link $active'>
                        <i class='far fa-circle nav-icon'></i>
                        <p>".ucfirst(str_replace("_"," ",$value))."</p>
                    </a>";

            echo $data;
        }
    }
?>
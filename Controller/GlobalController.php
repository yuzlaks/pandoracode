<?php

    @include 'GlobalVariable.php';
    @include '../views/setting/app.php?type_css=true';
    @include $_SERVER['DOCUMENT_ROOT'] . '/'.$PROJECTNAME.'/Config/database.php';
    
    $GLOBALS['projectname'] = $PROJECTNAME;
    $GLOBALS['location']    = $location;
    $GLOBALS['page']        = $page;

    #Declare Variable
    @$no           = 1;
    @$params       = $_GET['params'];
    @$request      = $_GET['request'];
    @$function     = $_GET['function'];
    @$id           = $_GET['id'];
    @$dataUpdate   = $_GET['data-update'];
    @$delete       = $_GET['delete'];
    @$mode         = $_GET['mode'];
    @$edit         = $_GET['edit'];
    @$table        = $_GET['table'];
    @$file         = $_GET['file'];
    @$nameOfFile   = $_GET['namefile'];
    @$anchor       = explode(".", $params);
    @$deleteMany   = $_GET['deleteMany'];
    @$columnTable  = $_GET['column-table'];
    @$targetFolder = $_GET['target-folder'];
    #End

    if (@$deleteMany) {
      return destroyMany($params, $request, $columnTable, $targetFolder);
    }

    if (isset($_POST['kirim'])) {
      $values = str_replace(",","','",$_POST['values']);

      return sendValues("'$values'");
    }

    if ($delete) {
        return destroy($table,$delete);
    }
    
    if ($edit) {
        return edit($table,$edit,$mode);
    }

    if (@$function == "update") {

        return update($params, rtrim($dataUpdate, ","), $id);
        
    }

    if (@$function == "tambah") {

        $removeChar    = str_replace("|","','",$request);
        $values        = "'$removeChar'";

        return create($params, $values, rtrim($dataUpdate, ","), $file, $nameOfFile);
        
    }

    function create($table, $data, $nameWithValue, $file = 0, $namefile = 0)
    { 
      
      $chekTable = mysqli_query($GLOBALS['koneksi'], "SELECT 1 FROM $table LIMIT 1");

      if ($chekTable == FALSE) {
        return errNotification($table,"Table dengan nama ".$table." tidak ditemukan !");
      }

      $query   = "INSERT INTO $table VALUES('',$data)";
      $command = mysqli_query($GLOBALS['koneksi'], $query);

      return notification($command, $table, "Berhasil Disimpan!", str_replace("'',","'id',",$query));

    }

    function edit($table,$id, $mode = 0)
    {

      $data    = "SELECT * FROM $table WHERE id = $id";
      $encode  = base64_encode($data);  
      header('Location:../views/'.$table.'/form.php?query='.$encode.'&id='.base64_encode($id).'&mode='.$mode);

    }

    function update($table,$data,$id)
    {
      $id         = base64_decode($id);
      $query      = "UPDATE $table SET $data WHERE id = $id";
      $command    = mysqli_query($GLOBALS['koneksi'], $query);

      return notification($command, $table, "Berhasil Diupdate!", $query);
      
    }

    function destroy($table, $id)
    {

      $query      = "DELETE FROM $table WHERE id = $id";
      $command    = mysqli_query($GLOBALS['koneksi'],$query);

      return notification($command, $table, "Berhasil Dihapus!", $query);
      
    }

    function destroyMany($table, $id, $columnTable = 0, $targetFolder = 0)
    {
      
      if (@$columnTable) {

        $get = rawQuery("SELECT * FROM $table WHERE id IN ($id)");
        while ($list = ambilData($get)) {

            @$getFile = $list["$columnTable"];
            @hapusFile(dir_public($targetFolder.'/'.$getFile));

        }

      }

      $query      = "DELETE FROM $table WHERE id IN ($id)";
      $command    = mysqli_query($GLOBALS['koneksi'],$query);

      return notification($command, $table, "Berhasil Dihapus!", $query);
    }

    function sendValues($params)
    {  
        $namatable  = $_POST['nama_tabel'];

        $chekTable = mysqli_query($GLOBALS['koneksi'], "SELECT 1 FROM $namatable LIMIT 1");

        if ($chekTable !== FALSE) {

            $field        = mysqli_query($GLOBALS['koneksi'],"SELECT * FROM $namatable");
            $countfield   = mysqli_num_fields($field);
            $valueOfField = str_replace(',',' ',$_POST['values']);
            $countword    = str_word_count($valueOfField);

            if ($countword > ($countfield - 1)) {
                echo "<br>
                <div class='container-fluid'>
                  <div class='alert alert-danger' role='alert' >
                    Data Value Melebihi Jumlah Kolom Tabel <b>".ucfirst($namatable)."</b>.
                  </div>
                </div>";
            }else{
                $insert    = "INSERT INTO $namatable VALUES('',$params)";
                mysqli_query($GLOBALS['koneksi'], $insert);
                header('Location: ' . $_SERVER['HTTP_REFERER']);
            }

        }else{
            echo "<br>
                <div class='container-fluid'>
                  <div class='alert alert-danger' role='alert' >
                    Tabel Dengan Nama <b>".ucfirst($namatable)."</b> Tidak Ada.
                  </div>
                </div>";
        }

    }

    function asset($params)
    {
      return "http://$_SERVER[HTTP_HOST]/$GLOBALS[projectname]/public/$params";
    }

    function dir_public($params)
    {
      return $_SERVER['DOCUMENT_ROOT'] . '/'.$GLOBALS['projectname'].'/public/'.$params;
    }

    function hapusFile($dir_public)
    {
      return unlink($dir_public);
    }

    function ambilTabel($params)
    {
       return mysqli_query($GLOBALS['koneksi'],"SELECT *,$params.id AS primary_id FROM $params ORDER BY id DESC");
    }

    function get($table)
    {
       return mysqli_fetch_assoc(mysqli_query($GLOBALS['koneksi'],"SELECT *,$table.id AS primary_id FROM $table ORDER BY id DESC"));
    }

    function relation($table,$relation, $select = null)
    {
       
       $primary    = $table.".".$relation."_id";
       $foreignKey = $relation.".id";
       $addSelect  = ",$table.id AS primary_id";

       $select == "" ? $select = "*,$table.id AS primary_id" : $select = $select.$addSelect;

       return mysqli_query($GLOBALS['koneksi'],"SELECT $select FROM $table INNER JOIN $relation ON $primary = $foreignKey  ORDER BY $table.id DESC");

    }

    function rawQuery($query)
    {
      return mysqli_query($GLOBALS['koneksi'],$query);
    }

    function ambilData($params)
    {
      return @mysqli_fetch_assoc($params);
    }

    function where($table, $id)
    {
      return mysqli_query($GLOBALS['koneksi'],"SELECT * FROM $table WHERE id = $id");
    }

    function hapusFileSebelumnya($variable, $namaFolder)
    {

      if (!$variable == '') {
          hapusFile(dir_public($namaFolder));
      }
      
    }

    function jika($variable, $maka, $jikaTidak)
    {
      return $variable ? $maka : $jikaTidak;
    }

    function jikaBenar($variable, $jalankan)
    {
      return $variable ?? $jalankan;
    }

    function jikaKosong($variable, $jalankan)
    {
      return $variable == '' ?? $jalankan;
    }
    
    function queryEdit($params)
    {
        return mysqli_query($GLOBALS['koneksi'],$params);
    }

    function route($function,$table, $id = 0)
    {
        return "http://$_SERVER[HTTP_HOST]/$GLOBALS[projectname]/views/setting/app.php?function=".$function."&params=".$table."&id=".$id;
    }

    function myController($controllerName,$functionName, $id = 0)
    {
        return "http://$_SERVER[HTTP_HOST]/$GLOBALS[projectname]/Controller/".$controllerName.".php?functionName=".$functionName."&id=".$id;
    }

    function deleteMany($table, $folder = 0, $columnTable = 0)
    {
       return "http://$_SERVER[HTTP_HOST]/$GLOBALS[projectname]/views/setting/app.php?params=".$table."&deleteMany=true&target-folder=".$folder."&column-table=".$columnTable;
    }

    function notification($command, $table, $msg, $query = 0)
    {
      if ($command) {
        echo '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">';
        echo '<div style="display:none"></div>';
        echo '<script type="text/javascript">
                  Swal.fire({
                      icon: "success",
                      title: "Berhasil!",
                      text: "Berhasil '.$msg.'",
                  }).then(function() {
                    window.location = "../views/'.$table.'/data.php";
                });
            </script>';
      }else{

        errMsg($query);

      }
    }

    function errNotification($table,$msg)
    {
        echo '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">';
        echo '<div style="display:none"></div>';
        echo '<script type="text/javascript">
                  Swal.fire({
                      icon: "error",
                      title: "Opps!",
                      text: "'.$msg.'",
                  }).then(function() {
                    window.location = "../views/'.$table.'/data.php";
                });
            </script>';
    }

    function tombolHapus($colspan)
    {
        $cek    = @ambilTabel($GLOBALS['page']);
        $hitung = @ambilData($cek);

        if ($hitung >=1 ) {

          $html = '<tfoot>
                      <tr>
                          <td colspan="'.$colspan.'"></td>
                          <td><button class="btn btn-danger" type="button" id="btn-delete"><i class="fa fa-trash"></i></button></td>
                      </tr>
                  </tfoot>';
        }

        return @$html;
    }

    function checkSama($list, $data)
    {
        if($list == $data){
          echo 'selected';
        }
    }

    function rupiah($params)
    {
      return "Rp " . number_format($params ,2,',','.');
    }

    function errMsg($msg)
    {

      $tableName = $_GET['params'];

      $value = $_GET['data-update'];

      if ($value == "") {
        $value = "<span style='letter-spacing: 0.1px;font-size:13px;background-color:ghostwhite;padding:3px;border-radius:5px'>- Tidak ada value yang dikirim -</span>";
      }else{
        $value = "<span style='letter-spacing: 0.1px;font-size:16px;background-color:ghostwhite;padding:3px;border-radius:5px'>$value</span>";
      }

      $query = $GLOBALS['koneksi']->query("SELECT DATA_TYPE,COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '$GLOBALS[database]' AND TABLE_NAME = '$tableName'");

      echo '<title>ERROR!</title>';

      echo '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">';
      echo '<body style="background-color:#eeeef5">';
      echo  '<div class="container-fluid" style="margin-top:30px">
                <div class="row">
                  <div class="col-md-10 offset-md-1">
                      <div class="card" style="background-color:white;border:none;padding:30px">
                        <div class="card" style="background-color:white;border:none">
                          <div class="card-header" style="background-color:white;border:none">
                            <span style="color:#9690a8;letter-spacing: 0,1em;">Sepertinya beberapa value tidak terkirim, silahkan cek query anda.</span>
                            <p style="color:#393457;font-size:19px"><b>Error Query : '.$msg.'</b></p>
                            <a style="color:#a9a4b8;text-decoration:none" href="../views/'.$tableName.'/form.php"><u>http://'.$_SERVER['HTTP_HOST'].'/'.$GLOBALS['projectname'].'/'.$tableName.'/form.php</u></a>
                          </div>
                        </div>
                    </div>
                    <div class="mt-4 card" style="border:none">
                      <div class="card-header" style="background-color:#4b486e;color:white;height:50px">
                        <h5 class="">Value</h5>
                      </div>
                      <div class="card-body">
                        <span style="color:#9690a8;letter-spacing: 0,1em;">Berikut ini adalah <strong>value</strong> yang dikirim ke tabel <code style="background-color:#f7f7f9;padding:4px">'.$tableName.'</code></span>
                        <p style="color:#393457;font-size:19px;"><b>'.rtrim($value,",").'  </b></p>
                      </div>
                    </div>
                    <div class="mt-4 card" style="border:none">
                      <div class="card-body">
                        <span style="color:#9690a8;letter-spacing: 0,1em;">Kami mengalami kesulitan dengan perintah query <code style="background-color:#f7f7f9;padding:4px">'.$_GET['function'].'</code>, berikut <strong>nama kolom</strong> & <strong>tipe data</strong> dari table <code style="background-color:#f7f7f9;padding:4px">'.$tableName.'</code></span>
                        <br><br>
                        <table class="table table-striped table-valign-middle table-bordered">
                          <thead>
                            <tr>
                              <th>Nama Kolom</th>
                              <th>Tipe Data</th>
                            </tr>
                          </thead>
                          <tbody>';

                              while($row = $query->fetch_assoc()){

                                echo "<tr>";
                                  echo "<td><code>$row[COLUMN_NAME]</code></td>";
                                  echo "<td>$row[DATA_TYPE]</td>";
                                echo "</tr>";

                                
                              }
                        
                    echo '</tbody>
                          </table>
                      </div>
                          </div>
                        </div>
                      </div>
                    </div>';

                    echo '</body>';

                   
    }
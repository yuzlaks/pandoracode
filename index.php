<?php

    include 'configuration.php';

    if ($REDIRECT) {
        header('location:views/'.$REDIRECT.'');
    }

    if ($AUTH) {
        session_start();
        if (empty($_SESSION['login'])) {
            header('location:Auth/login.php');
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Home</title>
</head>
<style>

    .center{text-align: center;}
    .title{
        margin-top: 50px;
        font-size: 30px;
        font-family: lato;
    }

    .text {
        background: linear-gradient(to right, #5b28ff 30%, #04c3ff 70%);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
    }

</style>
<body style="background-image: url('public/assets/bg.png');background-repeat: no-repeat;background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 center">
                
            <img src="public/assets/pandora.png" width="600px" style="margin-top: 210px;" alt="">
                
                <div class="typewriter">
                    <?php

                        $waktu = null;

                        if (date('H') > 24) {$waktu = "dini hari";}

                        if (date('H') > 3) {$waktu = "shubuh";}

                        if (date('H') > 4) {$waktu = "pagi";}

                        if (date('H') > 10) {$waktu = "siang";}

                        if (date('H') > 15) {$waktu = "sore";}

                        if (date('H') > 18) {$waktu = "petang";}

                        if (date('H') > 19) {$waktu = "malam";}

                    ?>
                    <br>
                    <h3>
                        <a href="" class="typewrite" data-period="2000" 
                            data-type='[ "Selamat <?= $waktu ?>", "Berkarya bersama pandoracode!", "Anda bisa klik mulai.", "Atau klik dokumentasi untuk melihat petunjuk." ]'>
                            <span class="wrap"></span>
                        </a>
                    </h3>

                </div>
                <?php
                    include 'views/setting/app.php';
                ?>
                <br><br>

                <center>
                    <a style="padding:7px;background-color: white;border-color:#2e86de;color:#2e86de" class="btn btn-sm btn-light" href="docs-page.php">Dokumentasi</a>
                    <a style="padding:7px;background-color: white;border-color:#2e86de;color:#2e86de" class="btn btn-sm btn-light ml-3" href="views/demo/data.php">Ayo Mulai !</a>
                </center>
            </div>
        </div>
    </div>
</body>
</html>
<script type="text/javascript">
    

var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        }

        setTimeout(function() {
        that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
              new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #00a8ff}";
        document.body.appendChild(css);
    };


</script>